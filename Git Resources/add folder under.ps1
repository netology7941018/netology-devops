$mains = Import-Csv $PSScriptRoot\mains.csv -Encoding Default
$mainPath = "$PSScriptRoot\..\Bonus\SQL"
$i = 1
function SET-Directory 
    {
    Param($name,$path)   
    $newPath = "$path\$name"
    New-Item -Name $Name -Path $Path -ItemType Directory
    New-Item -Name "README.md" -Path "$newPath" -ItemType File -Value $Name
    return $newPath
}

foreach($item in $mains){
    $MainName = $item.mains
    $newPath = SET-Directory -name $MainName -path $mainPath
    
        if($i -eq 1){
            $modules = Import-Csv $PSScriptRoot\modules1.csv -Encoding Default
            foreach($item in $modules){
                $modulename = $item.review
                SET-Directory -path $newPath -name $modulename 
            }
            
        }
        elseif($i -eq 2) {
            $modules = Import-Csv $PSScriptRoot\modules2.csv -Encoding Default
            foreach($item in $modules){
                $modulename = $item.linux
                SET-Directory -path $newPath -name $modulename 
            }
        }
        elseif($i -eq 3){
            $modules = Import-Csv $PSScriptRoot\modules3.csv -Encoding Default
            foreach($item in $modules){
                $modulename = $item.admin
                SET-Directory -path $newPath -name $modulename 
            }
        }
        else {
            Write-Host "End!" -ForegroundColor Green
        }
    $i = $i + 1
}
