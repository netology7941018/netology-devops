$modules = Import-Csv $PSScriptRoot\modules.csv -Encoding Default
$mainPath = "$PSScriptRoot\Main"
foreach($item in $modules){
    $newPath = $mainPath + "\" + $item.modules
    $module = $item.modules
    New-Item -Name $module -Path $mainPath -ItemType Directory -Verbose
    New-Item -Name "Readme.md" -Path "$newPath" -ItemType File -Value "Module - $module."
}
